# mbdecrypt

This python script can be used to decrypt entire mailboxes, either in mbox or in Maildir format, so that you can search them easily, with GnuPG.
Just pass it the type of mailbox (mbox or Maildir) you use and the paths to the input mailbox (with the encrypted mail) and the output mailbox (where the decrypted mail will be stored).
The script supports syncing with Message-IDs, meaning that you can keep on giving it the same mailboxes as arguments and it will only decrypt and store new email that hasn't already been decrypted in previous runs.

Please make sure that your output mailbox is only stored locally on your machine and not synced with your email provider, as otherwise all of your email will be on your provider's server unencrypted.

## Installation

Just put `mbdecrypt` into `/usr/local/bin/` or run it locally. Make sure that you have Python 3 and GnuPG installed.

## Examples

    $ mbdecrypt maildir ~/Mail/imap/INBOX ~/Mail/archive/decrypted
    $ mbdecrypt mbox inbox.mbox decrypted.mbox

## What is mbox? What is Maildir?

They are file formats for storing email. If you've never heard of them, look into the documentation of your email client. It probably supports exporting and importing at least one of the two. You can also use [OfflineIMAP](http://www.offlineimap.org/) to sync a Maildir directly with your IMAP server.